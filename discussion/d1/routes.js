const http = require("http");

// storing the 400 in a variable called port

const port = 4000;

// storing the createServer method inside the server variable

const server = http.createServer((request,response) => {
	// GET
	if (request.url === "/items" && request.method === "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database");
	};
	// POST
	if (request.url === "/items" && request.method === "POST") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data to be sent to the database");
	};
	// if (request.url === "/homepage") {
	// 	response.writeHead(200, {"Content-Type": "text/plain"});
	// 	response.end("Hello");
	// }
	// else {
	// 	response.writeHead(404, {"Content-Type": "text/plain"});
	// 	response.end("Page not found");	
	// }
});

server.listen(port);

console.log(`Server now running at port: ${port}`);