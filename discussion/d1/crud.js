const http = require("http");

// storing the 400 in a variable called port

const port = 4000;

// mock data base
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail"
	}
];

// storing the createServer method inside the server variable

const server = http.createServer((request, response) => {
	
	// route for returning all items upon receiving a GET method request

	if (request.url === "/users" && request.method === "GET") {
		response.writeHead(200, {"Content-Type": "application/json"});
		response.write(JSON.stringify(directory));
		response.end();
	}
	if (request.url === "/users" && request.method === "POST") {
		let requestBody = "";
		/*
			A stream is a sequence of data

			data is received from the client and is processed in the "data" stream
		*/
		request.on("data",function(data){
			// request end step = only runs after the request has completely been sent
			requestBody += data;
		});

		request.on("end",function(){
			// checks if at this point, the requestBody is data type STRING
			// needed type is JSON to access properties
			console.log(typeof requestBody);

			// converts the string requestBody to JSON 
			requestBody = JSON.parse(requestBody);

		// creating new object representing the new mock database record
		let newUser = {
			"name": requestBody.name,
			"email": requestBody.email
		};

		directory.push(newUser);
		console.log(directory);

		response.writeHead(200, {"Content-Type": "application/json"});
		response.write(JSON.stringify(newUser));
		response.end();
		});
	};
});

server.listen(port);

console.log(`Server now running at port: ${port}`);